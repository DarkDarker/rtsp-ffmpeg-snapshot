'use strict';

const express = require('express');
const path = require('path')
const { exec } = require('child_process');
const fs = require('fs');

// Constants
const FALLBACK_CAMERA_MAPPINGS = process.env.CAMERA_MAPPINGS || '[]'
const CAMERA_MAPPINGS = JSON.parse(FALLBACK_CAMERA_MAPPINGS);
const WORKDIR = process.env.WORKDIR || '/tmp';
const FFMPEG_CMD = process.env.FFMPEG_CMD || 'ffmpeg -y -i ${rtsp} -vframes 1 ${dir}/latest.jpg';
const PORT = parseInt(process.env.PORT) || 8151;
const HOST = '0.0.0.0';

function random() {
  const high = 30000;
  const low = 1;
  return Math.random() * (high - low) + low;
}

function makeRtspSnapshot(rtsp, name, timeoutSec) {
  const dir = WORKDIR + path.sep + name;
  const cmd = FFMPEG_CMD.replace('${rtsp}', rtsp).replace('${dir}', dir)

  !fs.existsSync(dir) && fs.mkdirSync(dir)

  exec(cmd, (err, out) => {
    if (err) {
      console.log(`Fail take snaphost on ${rtsp} for ${name}` + err);
    } else {
      console.log(`Sucessfull take snaphost on ${rtsp} for ${name}`);

    }
    setTimeout(function () {
      makeRtspSnapshot(rtsp, name, timeoutSec);
    }, timeoutSec * 1000 + random());
  });
}

// App
const app = express();
app.get('/snapshot/:name/latest', function (req, res) {
  res.sendFile(WORKDIR + path.sep + req.params.name + path.sep + 'latest.jpg')
});

app.listen(PORT, HOST);

console.log(`Running on http://${HOST}:${PORT}`);
console.log('Camera mappings:');
console.log(CAMERA_MAPPINGS);

CAMERA_MAPPINGS.forEach(function (mapping) {
  setTimeout(function () {
    makeRtspSnapshot(mapping.url, mapping.name, mapping.secs);
  }, random());

});

